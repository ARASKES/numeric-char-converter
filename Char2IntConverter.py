import sys

argument = sys.argv[1]
if len(argument) == 1 and argument.isdigit():
    output = int(argument)
else:
    output = "NaN"

print(output)
